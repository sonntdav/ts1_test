package cz.cvut.fel.ts1;

public class Sonntdav {
    public long factorial(int n){
        int result=1;
        int i=1;
        while(i<=n){
            result=result*i;
            i++;
        }

        return result;
    }
    public long factorialRecursive(int n) {
        if (n <= 1) {
            return 1;
        } else {
            return n * factorialRecursive(n - 1);
        }
    }
}
