package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class SonntdavTest {
    @Test
    public void factorialTest() {
        Sonntdav sonntdav  = new Sonntdav();
        int n = 2;
        long expectedResult = 2;

        long result = sonntdav.factorial(n);

        Assertions.assertEquals(expectedResult,result);
    }
    @Test
    public void factorialRecursiveTest(){
        Sonntdav sonntdav  = new Sonntdav();
        int n = 2;
        long expectedResult = 2;

        long result = sonntdav.factorialRecursive(n);

        Assertions.assertEquals(expectedResult,result);
    }
}
